import { publicServices } from "../services/index";
import swal from 'sweetalert'
import lodash from 'lodash'

function RegisterActions(params) {
    return dispatch => {
        publicServices.RegisterServices(params)
            .then((res) => {
                if (res.succces) {
                    localStorage.setItem('meAuth', res.data.data);
                    dispatch({
                        type: "REGISTER_SUCCESS",
                        data: res.data.data
                    })

                    swal("Register Successfully", "Let's enjoy", "success")
                        .then((value) => {
                            window.location.assign(`http://localhost:3000/`)
                        })
                } else {
                    swal("Register Failed", 'Error Message : ' + (res.errMsg), "error");
                }
            });
    };
}

function LoginActions(params) {
    return dispatch => {
        publicServices.Login(params)
            .then((res) => {
                if (res.succces) {
                    localStorage.setItem('meAuth', res.data.data);
                    dispatch({
                        type: "LOGIN_SUCCESS",
                        data: res.data.data
                    })
                    swal("Login Successfully", "Let's enjoy", "success")
                        .then((value) => {
                            window.location.assign(`http://localhost:3000/`)
                        })
                } else {
                    swal("Register Failed", 'Error Message : ' + (res.errMsg), "error");
                }
            });
    };
}

function GetHotelList(params) {
    return dispatch => {
        publicServices.GetHotelList(params)
            .then((res) => {
                if (res.succces) {
                    dispatch({
                        type: "GET_HOTEL_LIST_SUCCESS",
                        data: res.data.data
                    })
                } else {
                    if (lodash.isEqualWith(res.errMsg, "UN_AUTHORIZATION")) {
                        swal("Register Failed", 'Error Message : ' + (res.errMsg), "error");
                        localStorage.removeItem('meAuth')
                        window.location.assign(`http://localhost:3000/`)
                    }
                    dispatch({
                        type: "GET_HOTEL_LIST_FAILED",
                        data: []
                    })
                }
            });
    };
}

function GetReservationList(params) {
    return dispatch => {
        publicServices.GetReservationList(params)
            .then((res) => {
                if (res.succces) {
                    dispatch({
                        type: "GET_RESERVATION_LIST_SUCCESS",
                        data: res.data.data
                    })
                } else {
                    if (lodash.isEqualWith(res.errMsg, "UN_AUTHORIZATION")) {
                        swal("Register Failed", 'Error Message : ' + (res.errMsg), "error");
                        localStorage.removeItem('meAuth')
                        window.location.assign(`http://localhost:3000/`)
                    }
                    dispatch({
                        type: "GET_RESERVATION_LIST_FAILED",
                        data: []
                    })
                }
            });
    };
}

function ReservationActions(params) {
    return dispatch => {
        publicServices.ReservationServices(params)
            .then((res) => {
                if (res.succces) {
                    dispatch({
                        type: "BOOKING_SUCCESS",
                        data: res.data.data
                    })
                    swal("Booking Successfully", "Let's enjoy", "success")
                } else {
                    if (lodash.isEqualWith(res.errMsg, "UN_AUTHORIZATION")) {
                        swal("Register Failed", 'Error Message : ' + (res.errMsg), "error");
                        localStorage.removeItem('meAuth')
                        window.location.assign(`http://localhost:3000/`)
                    }
                    dispatch({
                        type: "BOOKING_FAILED",
                        data: []
                    })
                    swal("Booking Failed", 'Error Message : ' + (res.errMsg), "error")
                }
            });
    };
}


export const publicActions = {
    RegisterActions, LoginActions, GetHotelList, GetReservationList, ReservationActions
}