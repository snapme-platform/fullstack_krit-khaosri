import React from "react";
import {  reduxForm } from "redux-form";
import { connect } from "react-redux";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Divider from "@material-ui/core/Divider";
import TableBooking from "./TableBooking"
import Menu from "../../Components/Menu/Menu.component";

//import  renderTextField from '../../Components/InputForm/InputForm'

class Booking extends React.Component {
  

  setComponent = () => {
    return (
      <div>
        <Card className="cardBooking">
          <div className="divBooking">
            <h1>Your Reservation</h1>
            <Divider />
          </div>
          <div>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="stretch"
            >
              <Grid item item xs={12} md={12} lg={12}>
                 <TableBooking />
               </Grid>
            </Grid>
          </div>
        </Card>
      </div>
    );
  };

  render() {
    return (
      <>
        <Menu content={this.setComponent()} />
      </>
    );
  }
}

function mapStateToProps(state) {
  const { token } = state;
  return {
    token,
  };
}
export default connect(mapStateToProps)(
  reduxForm({
    form: "Booking",
  })(Booking)
);
