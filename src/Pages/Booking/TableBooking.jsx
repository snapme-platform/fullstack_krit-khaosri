import React from "react";
import { reduxForm } from "redux-form";
import { connect } from "react-redux";
import MUIDataTable from "mui-datatables";
import { publicActions } from "../../actions";

class HotelList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          name: "date_checkIn",
          label: "Check in",
          options: {
            filter: true,
            sort: false,
          },
        },
        {
          name: "date_checkOut",
          label: "Check out",
          options: {
            filter: true,
            sort: false,
          },
        },
        {
          name: "name",
          label: "Hostel Name",
          options: {
            filter: true,
            sort: true,
          },
        },
      ],
    };
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(publicActions.GetReservationList());
  }

  renderReservationList = () => {
    const { reservation_list } = this.props;
    const options = {
      filterType: "checkbox",
    };
    let reservation = [];
    let index = 0
    console.log(reservation_list);
    reservation.push(
      <MUIDataTable
        key={index}
        title={"Reservation List"}
        data={reservation_list}
        columns={this.state.columns}
        options={options}
      />
    );
    return reservation;
  };

  render() {
    return (
      <div className="layer1">
        <div className="layer2">{this.renderReservationList()}</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { reservation_list } = state.publicReducers;
  return { reservation_list };
}

export default connect(mapStateToProps)(
  reduxForm({
    form: "homeother",
  })(HotelList)
);
