import React from "react";
import Card from "@material-ui/core/Card";
import { reduxForm } from "redux-form";
import { connect } from "react-redux";

import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { publicActions } from "../../actions";
import { Grid } from "@material-ui/core";
import jwt from "jwt-decode";
import lodash from "lodash";
import swal from "sweetalert";

class HotelList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(publicActions.GetHotelList("?"));
  }

  handleBooking = (val) => {
    const user = jwt(localStorage.getItem("meAuth"));
    let data = {
      hotelId: val._id,
      userId: user._id,
      date_checkIn: this.props.date_checkIn,
      date_checkOut: this.props.date_checkOut,
    };

    swal("Comfirm for booking ?", {
      icon: "info",
      buttons: {
        cancel: "Cancel",
        delete: {
          text: "Book Now",
          value: "booking",
        },
      },
    })
      .then((value) => {
        switch (value) {

          case "booking":
            const { dispatch } = this.props;
            dispatch(publicActions.ReservationActions(data));
            break;

          default:
        }
      });
  };

  renderProduct = () => {
    const { hotel_list } = this.props;
    let product = [];
    hotel_list.forEach((val, index) => {
      product.push(
        <div key={index} className="divRenderHotel">
          <Card>
            <CardActionArea>
              <CardMedia title="Contemplative Reptile">
                <img
                  className="imgSize"
                  src={val.imgUrl}
                  alt="recipe thumbnail"
                />
              </CardMedia>
              <CardContent>
                <Typography gutterBottom variant="h5" component="h3">
                  <Grid
                    container
                    direction="row"
                    justify="space-between"
                    alignItems="baseline"
                  >
                    <Grid item item xs={12} md={6} lg={6}>
                      {val.name}
                    </Grid>
                    <Grid item item xs={12} md={6} lg={6}>
                      <Grid container justify="flex-end">
                        {val.price} {"฿"}
                      </Grid>
                    </Grid>
                  </Grid>
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {val.detail}
                </Typography>
              </CardContent>
            </CardActionArea>
            <CardActions>
              <Button
                onClick={() => window.open(val.location, "_blank")}
                size="small"
                color="primary"
              >
                Location
              </Button>
              <Button
                size="small"
                color="primary"
                onClick={() => {
                  this.handleBooking(val);
                }}
              >
                Book now
              </Button>
            </CardActions>
          </Card>
        </div>
      );
    });
    return product;
  };

  render() {
    const { hotel_list } = this.props;
    return (
      <div className="divLayer">
        {lodash.isEmpty(hotel_list) ? (
          <div className="divLayer2">
            <div className="divRenderHotel">
              <Card className="noData">
                <CardActionArea>
                  <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    <h2>no data mathing</h2>
                  </Grid>
                </CardActionArea>
              </Card>
            </div>
          </div>
        ) : (
          <div className="divLayer2">{this.renderProduct()}</div>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { hotel_list, date_checkIn, date_checkOut } = state.publicReducers;
  return { hotel_list, date_checkIn, date_checkOut };
}

export default connect(mapStateToProps)(
  reduxForm({
    form: "homeother",
  })(HotelList)
);
