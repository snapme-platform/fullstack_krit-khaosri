import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import Typography from "@material-ui/core/Typography";
import { publicActions } from "../../actions";
import HostelList from "./HotelList.pages";
import Menu from "../../Components/Menu/Menu.component";
import renderTextField from "../../Components/InputForm/InputForm";
import moment from 'moment'
const validate = values => {
  const errors = {}
  const requiredFields = [
    'date_checkOut',
    'date_checkIn'
  ]
 
  if(moment(values.date_checkIn).isAfter(values.date_checkOut)){
    errors.date_checkIn = 'Wrong date or format input'
    errors.date_checkOut = 'Wrong date format input'
  }
  if(moment(values.date_checkIn).isBefore(moment())){
    errors.date_checkIn = 'Can not select after today'
  }
  if(moment(values.date_checkOut).isBefore(moment())){
    errors.date_checkOut = 'Can not select after today'
  }

  return errors
}

class HomeSearch extends React.Component {
  constructor(props) {
    super(props);
  }
  handleSubmit = async (values) => {
    const { dispatch } = this.props;
    let param = await this.makeParams(values)
    dispatch(publicActions.GetHotelList(param));
  };

  makeParams = (params) => 
  { return params ? '?' + Object.keys(params).map((key) => ([key, params[key]].map(encodeURIComponent).join("="))).join("&") : '' }

  render() {
    const { handleSubmit } = this.props;
    return (
      <>
        <div className="leyer">
          <h3>เลือกรูปแบบตามความต้องการ</h3>
          <form onSubmit={handleSubmit(this.handleSubmit)}>
            <Grid
              container
              direction="row"
              justify="space-evenly"
              alignItems="center"
              spacing={3}
            >
              <Grid item xs={12} md={12} lg={6}>
              <label>
                <small>Check in date</small>
              </label>
                <Field
                  id="date_checkIn"
                  name="date_checkIn"
                  type="Date"
                  
                  margin="normal"
                  fullWidth
                  component={renderTextField}
                />
              </Grid>

              <Grid item xs={12} md={12} lg={6}>
              <label>
                <small>Check out date</small>
              </label>
                <Field
                  id="date_checkOut"
                  name="date_checkOut"
                  type="Date"
                  margin="normal"
                  fullWidth
                  component={renderTextField}
                />
              </Grid>
              <Grid item xs={12} md={12} lg={6}>
                <Field
                  id="keywords"
                  name="keywords"
                  type="text"
                  fullWidth
                  helperText="Search for Hostel"
                  margin="normal"
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <SearchIcon />
                      </InputAdornment>
                    ),
                  }}
                  className="fieldSearch"
                  component={renderTextField}
                /> 
              </Grid>
              <Grid item xs={12} md={12} lg={6}>
                <div className="divSubmitSearch">
                  <button className="buttonSubmitSearch" type="submit">
                    Submit
                  </button>
                </div>
              </Grid>
            </Grid>
          </form>
        </div>
      </>
    );
  }
}

function mapStateToProps(state) {
  const { token } = state;
  return {
    token,
  };
}
export default connect(mapStateToProps)(
  reduxForm({
    form: "HomeSearch",
    validate,
  })(HomeSearch)
);
