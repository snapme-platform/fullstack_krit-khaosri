import React from "react";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Card from "@material-ui/core/Card";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import Divider from "@material-ui/core/Divider";

import HostelList from "./HotelList.pages";
import Menu from "../../Components/Menu/Menu.component";
import HomeSearch from "./HomeSearch.pages"
//import  renderTextField from '../../Components/InputForm/InputForm'

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  setComponent = () => {
    return (
      <div>
        <Card className="cardBooking">
          <div className="divBooking">
            <h1>We Booking Hostel</h1>
            <Divider />
          </div>
          <div>
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="stretch"
            >
              <Grid item item xs={12} md={6} lg={7}>
                <HomeSearch/>
              </Grid>
              <Grid item item xs={12} md={6} lg={5}>
                <Grid container justify="flex-start">
                <HostelList />
                </Grid>
              </Grid>
            </Grid>
          </div>
        </Card>
      </div>
    );
  };

  

  render() {
    return (
      <>
        <Menu content={this.setComponent()} />
      </>
    );
  }
}

function mapStateToProps(state) {
  const { token } = state;
  return {
    token
  };
}
export default connect(mapStateToProps)(
  reduxForm({
    form: "Home",
  })(Home)
);
