import React from "react";
import TextField from "@material-ui/core/TextField";
import { Field, reduxForm } from "redux-form";
import { connect } from "react-redux";
import renderTextField from "../../Components/InputForm/InputForm";
import { publicActions } from "../../actions";
import lodash from "lodash";
import * as moment from 'moment';

const validate = (values) => {
  const errors = {};

  const requiredFields = [
    "name",
    "lastname",
    "email",
    "password",
    "confirmPasswords",
    "brith_date",
  ];
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = "Required";
    }
  });
  if (values.email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  if (lodash.isEqualWith(values.password, values.confirmPasswords) === false) {
    errors.password = "Password not match";
    errors.confirmPasswords = "Password not match";
  }
   if (values.brith_date!==null ) {
    let years = moment().diff(values.brith_date, 'years',true);
       if(parseInt(years) < 16)
       {
        errors.brith_date = "Minimum age is 15 ";
       }
  }
  if (lodash.get(values, "password.length") <8) {
    errors.password = "Passwords Charactor must be more than 8 digit";
  }
  if (lodash.get(values, "confirmPasswords.length") <8) {
    errors.confirmPasswords = "Passwords Charactor must be more than 8 digit";
  }

  return errors;
};

class Signup extends React.Component {
  constructor(props) {
    super(props);
  }

  handleSubmit = (values) => {
    const { dispatch } = this.props;
    dispatch(publicActions.RegisterActions(values));
  };


  goSignup =()=>{
    this.props.history.push('/login');
  }


  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="wrapper">
        <div className="form-wrapper">
          <h2 className="textLogin"> Sign Up </h2>
          <div>
            <h5 className="textLogin"> กรุณากรอกข้อมูลให้ถูกต้อง </h5>
          </div>
          <form onSubmit={handleSubmit(this.handleSubmit)}>
            <div className="name">
              <Field
                id="firstName"
                name="name"
                label="First Name"
                type="text"
                component={renderTextField}
              />
            </div>

            <div className="lastname">
              <Field
                id="lastName"
                name="lastname"
                label="Last Name"
                type="text"
                component={renderTextField}
              />
            </div>

            <div className="email">
              <Field
                id="email"
                name="email"
                label="Email"
                type="text"
                component={renderTextField}
              />
            </div>

            <div className="password">
              <Field
                id="password"
                name="password"
                label="Password"
                type="password"
                component={renderTextField}
              />
            </div>

            <div className="password">
              <Field
                id="confirmPasswords"
                name="confirmPasswords"
                label="Confirm Password"
                type="password"
                component={renderTextField}
              />
            </div>

            <div className="brithDate">
              <label>
                <small>Brith Date</small>
              </label>
             <Field
                id="brithDate"
                name="brith_date"
                type="date"        
                component={renderTextField}
              /> 
            </div>
            <div className="divSubmit">
              <button className="buttonSignUp" type="submit">
                Submit
              </button>
            </div>
          </form>

          <button onClick={()=>{ return this.goSignup()}} className="buttonBack" >Back</button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { token } = state;
  return {
    token,
  };
}
export default connect(mapStateToProps)(
  reduxForm({
    form: "signup",
    validate,
  })(Signup)
);
