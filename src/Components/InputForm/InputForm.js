import React from 'react'
import { TextField } from "@material-ui/core";

const renderTextField = ({
    label,
    input,
    defaultValue,
    meta: { touched, invalid, error },
    ...custom
  }) => (
      <TextField
        label={label}
        error={touched && invalid}
        helperText={touched && error}
        {...input}
        {...custom}
      />
    )
  
export default renderTextField;