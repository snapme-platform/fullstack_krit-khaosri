import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form'
import { publicReducers } from './public.reducer'

const reducer = combineReducers({
    form: formReducer,
    publicReducers
})

export default reducer;