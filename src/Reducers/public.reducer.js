const intitialState = {
    hotel_list: [],
    reservation_list: [],
    booking: [],
    date_checkIn: "",
    date_checkOut: "",
    me: [],
  };
  
  export function publicReducers(state = intitialState, action) {
    switch (action.type) {
      case "REGISTER_SUCCESS":
        return {
          ...state,
          token: action.data,
        };
  
      case "LOGIN_SUCCESS":
        return {
          ...state,
        };
  
      case "GET_HOTEL_LIST_SUCCESS":
        return {
          ...state,
          hotel_list: action.data.hotel_list ? action.data.hotel_list : [],
          date_checkIn: action.data.date_checkIn ? action.data.date_checkIn : [],
          date_checkOut: action.data.date_checkOut
            ? action.data.date_checkOut
            : [],
        };
  
      case "GET_HOTEL_LIST_FAILED":
        return {
          ...state,
          hotel_list: [],
        };
  
      case "GET_RESERVATION_LIST_SUCCESS":
        return {
          ...state,
          reservation_list: action.data ? action.data : [],
        };
  
      case "GET_RESERVATION_LIST_FAILED":
        return {
          ...state,
          reservation_list: [],
        };
  
      case "BOOKING_SUCCESS":
        return {
          ...state,
          booking: [],
        };
  
      case "BOOKING_FAILED":
        return {
          ...state,
          booking: [],
        };
  
      default:
        return state;
    }
  }
  